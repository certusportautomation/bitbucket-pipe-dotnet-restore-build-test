# Bitbucket Pipelines Pipe: dotnet-restore-build-test

Builds and tests the provided dotnet solution.

## Variables

| Variable              | Description           |
| --------------------- | ----------------------------------------------------------- |
| DEVOPS_APIKEY   		| ApiKey from Azure DevOps for pushing nuget packages |
| DEVOPS_URL   			| URL to Azure Devops 	|
| DOTNET_TEST_FILTER   	| For excluding unit tests	|
| NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED | Enables Session token cache |
| SOLUTION_FILEPATH     | Path to the solution file |
| VSS_NUGET_EXTERNAL_FEED_ENDPOINTS | Endpoint to the Azure DevOps environment |

## YAML Definition (Using repository variables)

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: docker://cepass/dotnet-restore-build-test
	variables:
	  DEVOPS_APIKEY: $DEVOPS_APIKEY
	  DEVOPS_URL: $DEVOPS_URL
	  DOTNET_TEST_FILTER: $DOTNET_TEST_FILTER
	  NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED: $NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
	  SOLUTION_FILEPATH: $SOLUTION_FILEPATH
	  VSS_NUGET_EXTERNAL_FEED_ENDPOINTS: $VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by pass@certusportautomation.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
