#!/usr/bin/env bash

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

printf "${HIGHLIGHT_YELLOW}Setting azure artifacts credential provider... \n"
export NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED=$NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
export VSS_NUGET_EXTERNAL_FEED_ENDPOINTS=$VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
wget -O - https://raw.githubusercontent.com/Microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh  | bash

printf "${HIGHLIGHT_YELLOW}Building an testing (also includes building nuget package(s))... \n"
dotnet restore -s $DEVOPS_URL -s "https://api.nuget.org/v3/index.json" $SOLUTION_FILEPATH
dotnet build --configuration Release $SOLUTION_FILEPATH
dotnet test --filter $DOTNET_TEST_FILTER $SOLUTION_FILEPATH

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code